import React from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CCollapse,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CFade,
  CForm,
  CFormGroup,
  CFormText,
  CValidFeedback,
  CInvalidFeedback,
  CTextarea,
  CInput,
  CInputFile,
  CInputCheckbox,
  CInputRadio,
  CInputGroup,
  CInputGroupAppend,
  CInputGroupPrepend,
  CDropdown,
  CInputGroupText,
  CLabel,
  CSelect,
  CRow,
  CSwitch
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { DocsLink } from 'src/reusable'

const BasicForms = () => {
  const [collapsed, setCollapsed] = React.useState(true)
  const [showElements, setShowElements] = React.useState(true)
  return (
    <>
    <CRow>
      <CCol xs="12" sm="12">
        <CCard>
          <CCardHeader>
            Users
            <small> Form</small>
          </CCardHeader>
          
          <CCardBody>
          <CRow>
          <CCol xs="12" sm="6">
            <CFormGroup>
              <CLabel htmlFor="firstName">First Name</CLabel>
              <CInput id="firstName" placeholder="Enter your First name" />
            </CFormGroup>
            </CCol>
            <CCol xs="12" sm="6">
            <CFormGroup>
              <CLabel htmlFor="middleName">Middle Name</CLabel>
              <CInput id="middleName" placeholder="Enter your Middle name" />
            </CFormGroup>
            </CCol>
            </CRow>
            <CRow>
          <CCol xs="12" sm="6">
            <CFormGroup>
              <CLabel htmlFor="lastName">Last Name</CLabel>
              <CInput id="lastName" placeholder="Enter your Last name" />
            </CFormGroup>
            </CCol>
            <CCol xs="12" sm="6">
            <CFormGroup>
              <CLabel htmlFor="email">Email</CLabel>
              <CInput id="email" placeholder="Enter your Email Address" />
            </CFormGroup>
            </CCol>
            </CRow>
            <CRow>
          <CCol xs="12" sm="6">
            <CFormGroup>
              <CLabel htmlFor="idNumber">Id Number</CLabel>
              <CInput id="idNumber" placeholder="Enter Your Id Number" />
            </CFormGroup>
            </CCol>
            <CCol xs="12" sm="6">
            <CFormGroup>
              <CLabel htmlFor="address">Address</CLabel>
              <CInput id="address" placeholder="Enter your Address" />
            </CFormGroup>
            <div>
            <CButton block color="primary">Submit</CButton>
                    
                </div>
            </CCol>
            </CRow>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
    </>
    )
    }
    export default BasicForms